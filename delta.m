function delta(mu)
% Disable error messages
warning off
% Choose sensible values for a and Emax
a=2; % in Angstrons
Emax=100; % in eV
% Define some fundamental constants
hbar=1.055e-34;
mass=9.11e-31;
% Convert eV and Angstroms into SI units
a=a * 1e-10;
Emax=Emax * 1.6e-19;
% Create a large vector of energies ranging from 0 to Emax
E=linspace(0,Emax,1e5);
% Compute the quantity Q and the right-hand-side
Q=sqrt(2*mass*E)/hbar;
rhs=cos(Q*a)+ mu*sin(Q*a)./(Q*a);
% Plot the rhs vs Energy (using eV for the latter)
subplot(2,1,1)
plot(E/1.6e-19,rhs)
xlabel('Energy (eV)')
ylabel('Right-hand side')
title(['Allowed and Forbidden Solutions for mu=' num2str(mu)])
%axis([-Inf +Inf -1 1]) % only plot y-values between -1 and +1
% Compute and plot the dispersion relation
ka=acos(rhs);
subplot(2,1,2)
plot(ka,E/1.6e-19,-ka,E/1.6e-19)
xlabel('ka')
ylabel('Energy (eV)')
title('Dispersion relation')

axis([-3.5 3.5 0 +Inf])